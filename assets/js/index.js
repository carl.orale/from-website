(function () {
  // ODOMETER
  var animateOdometerUp,
    animateOdometerDown,
    animate,
    init,
    odometerLeft,
    odometerRight,
    odometerLeftReverse,
    odometerRightReverse;

  const section04 = {
    LG: {
      paragraph1: 2700,
      paragraph2: 2850,
      paragraph3: 3000,
      paragraph4: 3150,
      paragraph5: 3300,
    },
  };

  const section07 = {
    LG: {
      subParagraph1: 3850,
      subParagraph2: 4000,
      subParagraph3: 4150,
      subParagraph4: 4300,
    },
  };

  animateOdometerUp = function () {
    return $(".odometer.goUp").addClass(
      "odometer-animating-up odometer-animating"
    );
  };

  animateOdometerDown = function () {
    return $(".odometer.goDown").addClass(
      "odometer-animating-down odometer-animating"
    );
  };

  animate = function () {
    animateOdometerUp();
    animateOdometerDown();
  };

  let count = 0;

  odometerLeft = function () {
    var rollItemCount = $(".goDown .odometer-ribbon-inner").children().length;
    percentage = -100 / (rollItemCount - 1);
    count++;
    step = Math.round(count / 14);
    if (step < 10 && step > 0) {
      move = step * percentage;
      movingto = move + "%";
      $(".goDown .odometer-ribbon-inner").css({
        transform: "translateY(" + movingto + ")",
        transition: "transform 2s",
      });
    }
  };

  odometerLeftReverse = function () {
    var rollItemCount = $(".goDown .odometer-ribbon-inner").children().length;
    percentage = -100 / (rollItemCount - 1);
    count--;
    step = Math.round(count / 14);
    if (step < 10 && step >= 0) {
      move = step * percentage;
      movingto = move + "%";
      $(".goDown .odometer-ribbon-inner").css({
        transform: "translateY(" + movingto + ")",
        transition: "transform 2s",
      });
    }
  };

  odometerRight = function () {
    var rollItemCount = $(".goUp .odometer-ribbon-inner").children().length;
    percentage = 100 / (rollItemCount - 1);
    count++;
    step = Math.round(count / 14);
    if (step < 10 && step > 0) {
      subs = percentage * step;
      move = subs + -100;
      movingto = move + "%";
      $(".goUp .odometer-ribbon-inner").css({
        transform: "translateY(" + movingto + ")",
        transition: "transform 2s",
      });
    }
  };

  odometerRightReverse = function () {
    var rollItemCount = $(".goUp .odometer-ribbon-inner").children().length;
    percentage = 100 / (rollItemCount - 1);
    count--;
    step = Math.round(count / 14);
    if (step < 10 && step >= 0) {
      subs = percentage * step;
      move = subs + -100;
      movingto = move + "%";
      $(".goUp .odometer-ribbon-inner").css({
        transform: "translateY(" + movingto + ")",
        transition: "transform 2s",
      });
    }
  };

  init = function () {
    return setTimeout(function () {
      return animate();
    }, 500);
  };

  init();

  const paragraph1 = document.getElementById("p1");
  const paragraph2 = document.getElementById("p2");
  const paragraph3 = document.getElementById("p3");
  const paragraph4 = document.getElementById("p4");
  const paragraph5 = document.getElementById("p5");

  const parentElementSect2 = document.getElementById("section02");
  const parentElementSect3 = document.getElementById("section03");
  const parentElementSect4 = document.getElementById("section04");
  const parentElementSect5 = document.getElementById("section05");
  const parentElementSect7 = document.getElementById("section07");
  const parentElementSect8 = document.getElementById("section08");
  const parentElementSect9 = document.getElementById("section09");
  const parentElementSect10 = document.getElementById("section10");
  const parentElementSect11 = document.getElementById("section11");

  const header1 = document.getElementById("heading1");
  const header2 = document.getElementById("heading2");
  const header3 = document.getElementById("heading3");
  const header4 = document.getElementById("heading4");
  const header5 = document.getElementById("heading5");

  const subParagraph1 = document.getElementById("subParagraph1");
  const subParagraph2 = document.getElementById("subParagraph2");
  const subParagraph3 = document.getElementById("subParagraph3");
  const subParagraph4 = document.getElementById("subParagraph4");

  function getOffset(el) {
    const rect = el.getBoundingClientRect();
    return {
      left: rect.left + window.scrollX,
      top: rect.top + window.scrollY,
    };
  }

  scrolled = function (scrollTop, width) {
    console.log("normal");
    if (width > 576) {
      if (scrollTop < getOffset(parentElementSect7).top) {
        subParagraph1.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph1.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect7).top &&
        scrollTop < getOffset(parentElementSect7).top + 150
      ) {
        subParagraph2.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph2.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect7).top + 300 &&
        scrollTop < getOffset(parentElementSect7).top + 450
      ) {
        subParagraph3.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph3.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect7).top + 450 &&
        scrollTop < getOffset(parentElementSect7).top + 600
      ) {
        subParagraph4.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph4.style.setProperty("display", "inline-block");
      }
      if (scrollTop > getOffset(parentElementSect2).top - 500) {
        $(".text-love").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
        $(".hiddenLine").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect3).top + 200) {
        $(".text-elegantly").animate(
          {
            "stroke-dashoffset": 0,
          },
          2000
        );
      }
      if (scrollTop > getOffset(parentElementSect3).top + 500) {
        $(".text-tech").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
        $(".hiddenLine-tech").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect5).top - 300) {
        $(".text-success").animate(
          {
            "stroke-dashoffset": 0,
          },
          3300
        );
        $(".hiddenLine-success").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect7).top - 200) {
        $(".text-proven").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect7).top + 500) {
        $(".text-transform").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect8).top - 400) {
        $(".text-family").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect9).top - 400) {
        $(".text-impact").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect10).top - 400) {
        $(".text-articles").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect11).top - 500) {
        $(".text-working").animate(
          {
            "stroke-dashoffset": 0,
          },
          2500
        );
        $(".hiddenLine-working").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
    } else {
      if (scrollTop < getOffset(parentElementSect7).top + 300) {
        subParagraph1.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph1.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect7).top + 300 &&
        scrollTop < getOffset(parentElementSect7).top + 300 + 150
      ) {
        subParagraph2.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph2.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect7).top + 300 + 300 &&
        scrollTop < getOffset(parentElementSect7).top + 300 + 450
      ) {
        subParagraph3.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph3.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect7).top + 300 + 450 &&
        scrollTop < getOffset(parentElementSect7).top + 300 + 600
      ) {
        subParagraph4.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        subParagraph4.style.setProperty("display", "inline-block");
      }

      if (scrollTop > getOffset(parentElementSect2).top) {
        $(".text-love").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
        $(".hiddenLine").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect3).top + 800) {
        $(".text-elegantly").animate(
          {
            "stroke-dashoffset": 0,
          },
          2000
        );
      }
      if (scrollTop > getOffset(parentElementSect3).top + 1500) {
        $(".text-tech").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
        $(".hiddenLine-tech").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect5).top - 200) {
        $(".text-success").animate(
          {
            "stroke-dashoffset": 0,
          },
          3300
        );
        $(".hiddenLine-success").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect7).top - 100) {
        $(".text-proven").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect7).top + 1000) {
        $(".text-transform").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect8).top - 400) {
        $(".text-family").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect9).top - 400) {
        $(".text-impact").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect10).top - 400) {
        $(".text-articles").animate(
          {
            "stroke-dashoffset": 0,
          },
          1500
        );
      }
      if (scrollTop > getOffset(parentElementSect11).top - 500) {
        $(".text-working").animate(
          {
            "stroke-dashoffset": 0,
          },
          2500
        );
        $(".hiddenLine-working").animate(
          {
            width: "0px",
            left: "100%",
          },
          1500
        );
      }
    }

    // sect4 mobile
    if (width > 576) {
      if (scrollTop < getOffset(parentElementSect4).top - 500) {
        header1.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 150
      ) {
        header2.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 150 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 300
      ) {
        header3.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 300 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 450
      ) {
        header4.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 450 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 600
      ) {
        header5.classList.add("addColorPurple");
      }
      if (scrollTop < getOffset(parentElementSect4).top - 500) {
        paragraph1.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 150
      ) {
        paragraph1.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "none");
        paragraph2.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "block");
      } else {
        paragraph2.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 150 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 300
      ) {
        paragraph3.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "block");
      } else {
        paragraph3.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 300 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 450
      ) {
        paragraph5.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "none");
        paragraph4.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "block");
      } else {
        paragraph4.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "none");
      }
      if (scrollTop > getOffset(parentElementSect4).top - 500 + 450) {
        paragraph5.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "block");
      } else {
        paragraph5.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "none");
      }
    } else {
      if (scrollTop < getOffset(parentElementSect4).top - 400) {
        header1.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 150
      ) {
        header2.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 150 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 300
      ) {
        header3.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 300 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 450
      ) {
        header4.classList.add("addColorPurple");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 450 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 600
      ) {
        header5.classList.add("addColorPurple");
      }

      if (scrollTop < getOffset(parentElementSect4).top - 400) {
        paragraph1.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 150
      ) {
        paragraph1.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "none");
        paragraph2.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "block");
      } else {
        paragraph2.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 150 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 300
      ) {
        paragraph3.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "block");
      } else {
        paragraph3.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 300 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 450
      ) {
        paragraph5.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "none");
        paragraph4.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "block");
      } else {
        paragraph4.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "none");
      }
      if (scrollTop > getOffset(parentElementSect4).top - 400 + 450) {
        paragraph5.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "block");
      } else {
        paragraph5.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "none");
      }
    }
  };

  scrolledReverse = function (scrollTop, width) {
    console.log("reverse");
    if (width > 576) {
      if (scrollTop < getOffset(parentElementSect4).top - 500) {
        paragraph1.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 150
      ) {
        paragraph1.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "none");
        paragraph2.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "block");
      } else {
        paragraph2.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 150 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 300
      ) {
        paragraph3.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "block");
      } else {
        paragraph3.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 500 + 300 &&
        scrollTop < getOffset(parentElementSect4).top - 500 + 450
      ) {
        paragraph4.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "block");
      } else {
        paragraph4.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "none");
      }
      if (scrollTop > getOffset(parentElementSect4).top - 500 + 450) {
        paragraph5.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "block");
      } else {
        paragraph5.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "none");
      }
    } else {
      if (scrollTop < getOffset(parentElementSect4).top - 400) {
        paragraph1.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "block");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 150
      ) {
        paragraph1.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph1.style.setProperty("display", "none");
        paragraph2.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "block");
      } else {
        paragraph2.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph2.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 150 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 300
      ) {
        paragraph3.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "block");
      } else {
        paragraph3.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph3.style.setProperty("display", "none");
      }
      if (
        scrollTop > getOffset(parentElementSect4).top - 400 + 300 &&
        scrollTop < getOffset(parentElementSect4).top - 400 + 450
      ) {
        paragraph4.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "block");
      } else {
        paragraph4.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph4.style.setProperty("display", "none");
      }
      if (scrollTop > getOffset(parentElementSect4).top - 400 + 450) {
        paragraph5.classList.add(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "block");
      } else {
        paragraph5.classList.remove(
          "animate__animated",
          "animate__fadeInDown",
          "animate__faster"
        );
        paragraph5.style.setProperty("display", "none");
      }
    }
  };

  var lastScrollTop = 0;
  let width = 0;

  $(window).scroll(function () {
    var st = $(this).scrollTop();
    width = $(document).width();
    console.log("lastScrollTop:" + lastScrollTop + " st:" + st);

    if (st > lastScrollTop) {
      // downscroll code
      odometerLeft();
      odometerRight();
      scrolled(st, width);
    } else if (st === 0) {
      // reach top
      $(".goUp .odometer-ribbon-inner").css({
        transform: "translateY(-100%)",
        transition: "transform 2s",
      });

      $(".goDown .odometer-ribbon-inner").css({
        transform: "translateY(0%)",
        transition: "transform 2s",
      });
    } else {
      // upscroll code
      odometerLeftReverse();
      odometerRightReverse();
      scrolledReverse(st, width);
    }

    lastScrollTop = st <= 0 ? 0 : st;
  });

  // TYPE WRITE
  const industry = document.getElementById("industry");

  var typewriter = new Typewriter(industry, {
    loop: true,
    delay: 100,
  });

  typewriter
    .pauseFor(500)
    .typeString("B2B")
    .pauseFor(500)
    .deleteAll()
    .pauseFor(500)
    .typeString("B2C")
    .pauseFor(500)
    .deleteAll()
    .pauseFor(500)
    .typeString("B2G")
    .pauseFor(500)
    .deleteAll()
    .pauseFor(500)
    .typeString("B2B2C")
    .pauseFor(500)
    .deleteAll()
    .pauseFor(500)
    .typeString("B2G")
    .pauseFor(500)
    .deleteAll()
    .pauseFor(500)
    .typeString("B2XYZ")
    .pauseFor(500)
    .deleteAll()
    .start();

  const technologies = document.getElementById("technologies");

  var typewriter2 = new Typewriter(technologies, {
    loop: true,
    delay: 75,
  });

  typewriter2
    .pauseFor(1500)
    .typeString("Machine Learning")
    .pauseFor(2000)
    .deleteAll()
    .pauseFor(1500)
    .typeString("Machine Learning")
    .pauseFor(2000)
    .deleteAll()
    .start();

  // window.addEventListener("scroll", onScroll);

  $(".workBox").hover(
    function () {
      let id = $(this).data("id");
      console.log($(this).attr("data-id"));

      $(".workwithus ul > li[data-id='" + id + "'] > div").css({
        "border-bottom-left-radius": "0",
        "border-bottom-right-radius": "0",
      });
    },
    () => {
      $(".workwithus ul > li > div").css({
        "border-bottom-left-radius": "50%",
        "border-bottom-right-radius": "50%",
      });
    }
  );

  var childHeight = $( document ).height();
  $(window).on('resize', function() {
    childHeight = $( document ).height();
  });

  // Listen for postMessage events.
  window.addEventListener("message", receiveMessage, false);

  // Storing parent message event in order to establish two-way communication.
  var parentMessageEvent;

  function receiveMessage(event) {
    // Make sure the sender of this message is who we think it is.
    if (event.origin !== 'https://qa.from.digital' &&
        event.origin !== 'https://qa.movingdigitalconsulting.com' && 
        event.origin !== 'https://www.from.digital') {
        return;
    }
    var object = JSON.parse(event.data);
    
    // Store parent message event for two-way communication
    parentMessageEvent = event;
    console.log('PARENT request event: ', childHeight);
    // console.log(event);
    if (object.event === 'requestNewHeight') {
      sendNewHeightToParentWindow();
    }
  }

  function sendNewHeightToParentWindow() {
    var lastContainerPosition = $('#section12 .cardContainer[data-id=3]').height() + $('#section12 .cardContainer[data-id=3]').offset().top + 100;
    var height = Math.min($( document ).height(), lastContainerPosition);
    console.log(`Global height var:  ${childHeight} - height sent to parent: ${height}`);

    if (parentMessageEvent != undefined) {
      parentMessageEvent.source.postMessage(JSON.stringify({
        event: 'updateHeight',
        height: height
      }), parentMessageEvent.origin);
    }
  };
}.call(this));
